from django.contrib import admin
from mptt.admin import MPTTModelAdmin

from .models import Menu, MenuItem


class MenuAdmin(admin.ModelAdmin):
    list_display = ('title',)
    list_filter = ['created', ]


class MenuItemAdmin(MPTTModelAdmin):
    list_display = ('title', 'path', 'position', 'target', 'menu')
    list_filter = ['menu__title', 'created']
    mptt_level_indent = 20


admin.site.register(Menu, MenuAdmin)
admin.site.register(MenuItem, MenuItemAdmin)
