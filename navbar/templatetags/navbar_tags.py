from django import template
from django.core import urlresolvers

from ..models import MenuItem

register = template.Library()


@register.tag
def navbar_menu(parser, token):
    template_name = 'menu.html'

    tokens = token.split_contents()
    num = len(tokens)

    if num not in [2, 3]:
        raise template.TemplateSyntaxError('%r tag requires two arguments.' % tokens[0])

    tree_query_set = MenuItem.objects.filter(menu__title=tokens[1][1:-1])

    if num == 3:
        template_name = tokens[2][1:-1]

    return Menu(tree_query_set, template_name)


@register.tag
def navbar_breadcrumbs(parser, token):
    template_name = 'breadcrumbs.html'

    return Breadcrumb(template_name)


@register.simple_tag(takes_context=True)
def is_active(context, url_name, return_value=' active', **kwargs):
    matches = current_url_equals(context, url_name, **kwargs)
    return return_value if matches else ''


def current_url_equals(context, url_name, **kwargs):
    resolved = False

    # TODO: url check
    try:
        resolved = urlresolvers.resolve(context.get('request').path)
    except:
        pass

    matches = resolved and resolved.url_name == url_name

    if matches and kwargs:
        for key in kwargs:
            kwarg = kwargs.get(key)
            resolved_kwarg = resolved.kwargs.get(key)
            if not resolved_kwarg or kwarg != resolved_kwarg:
                return False

    return matches


class Menu(template.Node):
    def __init__(self, tree_query_set, template_name):
        self.tree_query_set = tree_query_set
        self.template_name = template_name

    def render(self, context):
        t = context.template.engine.get_template(self.template_name)
        return t.render(
            template.Context({
                'tree_menu': self.tree_query_set,
            }, autoescape=context.autoescape))


class Breadcrumb(template.Node):
    def __init__(self, template_name):
        self.template_name = template_name

    def render(self, context):
        t = context.template.engine.get_template(self.template_name)
        return t.render(
            template.Context({
            }, autoescape=context.autoescape)
        )
