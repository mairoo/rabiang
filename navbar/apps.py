from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class NavbarConfig(AppConfig):
    name = 'navbar'
    verbose_name = _('Navbar')
