from django.db import models
from django.utils.translation import ugettext_lazy as _
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel

from model_utils.models import TimeStampedModel


class MenuItem(TimeStampedModel, MPTTModel):
    TARGET_CHOICES = (
        ('_self', _('_self')),
        ('_blank', _('_blank')),
    )

    menu = models.ForeignKey(
        'Menu',
        help_text=_('Menu name'),
        verbose_name=_('Menu'),
        related_name='menu'
    )
    parent = TreeForeignKey(
        'self',
        verbose_name=_('Parent'),
        blank=True,
        null=True,
        related_name='children',
        db_index=True
    )
    title = models.CharField(
        verbose_name=_('Title'),
        help_text=_('Menu item title which can be translated '),
        max_length=128
    )
    path = models.CharField(
        verbose_name=_('Path'),
        help_text=_('Exact URL or URL pattern'),
        max_length=256
    )
    is_url_pattern = models.BooleanField(
        verbose_name=_('Is URL pattern'),
        help_text=_('Whether the given URL should be treated as a pattern.'),
        default=False
    )
    target = models.CharField(
        verbose_name=_('Target'),
        help_text=_('Link target option'),
        max_length=10,
        choices=TARGET_CHOICES,
        default='_self'
    )
    position = models.IntegerField(
        verbose_name=_('Position'),
        help_text=_('Menu item position'),
        default=1,
    )

    class Meta:
        verbose_name = _('menu item')
        verbose_name_plural = _('menu items')
        ordering = ['tree_id', 'lft']

    class MPTTMeta:
        order_insertion_by = ['position']

    def __str__(self):
        return self.title


class Menu(TimeStampedModel):
    title = models.CharField(
        verbose_name=_('Title'),
        help_text=_('Menu title'),
        max_length=128
    )

    class Meta:
        verbose_name = _('menu')
        verbose_name_plural = _('menu')
        ordering = ('-created',)

    def __str__(self):
        return self.title
