# Goals

* Learning Django
* Learning Pythonic way of coding

# Apps

## Blog

* 2-cols sidebar
* Tagging - django-tagging
* Comments and Comment moderation - django-disqus
* Categories
* WYSIWYG editor - django-summernote
* Date-based archives
* RSS feed
* Similar posts
* Markdown and various markup types
* Image/file upload
* Search
* Trackback / Pingback
* Multi Author
* Akismet Spam Filtering or ReCAPTCHA

## Board

## Page

## Account