from django.db import models
from django.utils.translation import ugettext_lazy as _


class TimeStampedModel(models.Model):
    created = models.DateTimeField(
        verbose_name=_('Date created'),
        auto_now_add=True
    )
    updated = models.DateTimeField(
        verbose_name=_('Date updated'),
        auto_now=True
    )

    class Meta:
        abstract = True
