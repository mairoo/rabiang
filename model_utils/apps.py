from django.apps import AppConfig


class ModelUtilsConfig(AppConfig):
    name = 'model_utils'
