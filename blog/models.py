from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _
from easy_thumbnails.fields import ThumbnailerImageField
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel
from tagging.fields import TagField

from model_utils.models import TimeStampedModel
from .managers import PostManager


class Post(TimeStampedModel):
    DRAFT = 0
    PUBLISHED = 1

    STATUS_CHOICES = (
        (DRAFT, _('Draft')),
        (PUBLISHED, _('Published')),
    )

    title = models.CharField(
        verbose_name=_('Title'),
        help_text=_('Enter blog post title.'),
        max_length=250
    )
    slug = models.SlugField(
        verbose_name=_('Slug'),
        help_text=_('Enter blog post slug.'),
        max_length=250,
        unique=False,
        allow_unicode=True
    )
    category = TreeForeignKey(
        'Category',
        verbose_name=_('Category'),
        help_text=_('Enter blog post category.'),
        null=True,
        blank=True,
        db_index=True
    )
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('Author'),
        help_text=_('Specify blog post author.'),
        related_name='blog_posts'
    )
    description = models.TextField(
        verbose_name=_('Description'),
        help_text=_("Enter blog post description which doesn't contain HTML tags"),
        blank=True
    )
    content = models.TextField(
        verbose_name=_('Content'),
        help_text = _('Enter blog post content.'),
    )
    date_published = models.DateTimeField(
        verbose_name=_('Date published'),
        help_text=_('Enter blog post published date.'),
        null=True
    )
    view_count = models.PositiveIntegerField(
        verbose_name=_('View Count'),
        default=0
    )
    ip_address = models.GenericIPAddressField(
        verbose_name=_('IP Address')
    )
    status = models.IntegerField(
        verbose_name=_('Status'),
        help_text=_('Specify blog post status.'),
        choices=STATUS_CHOICES,
        default=DRAFT
    )
    allow_comments = models.BooleanField(
        verbose_name=_('Allow comments'),
        default=True
    )
    tag = TagField(
        verbose_name=_('Tag')
    )
    thumbnail = ThumbnailerImageField(
        verbose_name=_('Thumbnail'),
        upload_to='blog/thumbnails/%Y/%m',
        blank=True
    )

    objects = PostManager()

    class Meta:
        verbose_name = _('post')
        verbose_name_plural = _('posts')
        ordering = ('-date_published',)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('blog:post-detail', args=(self.pk, self.slug,))

    def get_previous_post(self):
        previous_post = Post.objects.published() \
            .filter(id__lt=self.id) \
            .order_by('-date_published').first()

        if previous_post is None:
            return None

        return reverse('blog:post-detail', args=(previous_post.pk, previous_post.slug,))

    def get_next_post(self):
        next_post = Post.objects.published() \
            .filter(id__gt=self.id) \
            .order_by('-date_published').last()

        if next_post is None:
            return None

        return reverse('blog:post-detail', args=(next_post.pk, next_post.slug,))


class Image(TimeStampedModel):
    post = models.ForeignKey(
        Post,
        verbose_name=_('Post'),
        related_name='blog_posts'
    )
    image = models.ImageField(
        verbose_name=_('Image'),
        upload_to='blog/images/%Y/%m'
    )

    class Meta:
        verbose_name = _('image')
        verbose_name_plural = _('images')
        ordering = ['created']

    def __str__(self):
        return self.image.name


class Category(TimeStampedModel, MPTTModel):
    parent = TreeForeignKey(
        'self',
        verbose_name=_('Parent'),
        blank=True,
        null=True,
        related_name='children',
        db_index=True
    )
    title = models.CharField(
        verbose_name=_('Title'),
        max_length=128
    )
    slug = models.SlugField(
        verbose_name=_('Slug'),
        max_length=250,
        unique=True,
        allow_unicode=True
    )
    position = models.IntegerField(
        verbose_name=_('Position'),
        default=1,
    )

    class Meta:
        verbose_name = _('category')
        verbose_name_plural = _('categories')
        ordering = ['tree_id', 'lft']

    class MPTTMeta:
        order_insertion_by = ['position']

    def __str__(self):
        return self.title
