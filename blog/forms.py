from django import forms
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from rabiang_editor.widgets import SimpleMDEWidget

from .models import Post


class PostSearchForm(forms.Form):
    q = forms.CharField(
        label=_('Search Word'),
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Search Word'),
                'required': 'True',
            }
        )
    )


class PostAdminForm(forms.ModelForm):
    class Meta:
        model = Post
        exclude = []
        options = getattr(settings, "SIMPLEMDE_OPTIONS", '')
        widgets = {
            'content': SimpleMDEWidget(wrapper_class='simplemde-box-admin', options=options),
        }
