import logging

from django.conf import settings
from django.contrib.syndication.views import Feed
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from django.views.generic import (
    ListView, DetailView, TemplateView
)
from django.views.generic.dates import (
    ArchiveIndexView, YearArchiveView, MonthArchiveView, DayArchiveView
)
from tagging.views import TaggedObjectList

from .models import (
    Post, Category
)
from .viewmixins import (
    PageableMixin, SearchContextMixin, CategoryContextMixin
)


class PostListView(PageableMixin, SearchContextMixin, CategoryContextMixin, ListView):
    logger = logging.getLogger(__name__)
    context_object_name = 'posts'
    template_name = 'blog/post_list.html'

    def get_queryset(self):
        self.logger.debug('PostListView.get_queryset()')
        form = self.form_class(self.request.GET)
        queryset = Post.objects.published().select_related('author').select_related('category')

        if form.is_valid():
            q = form.cleaned_data['q']
            queryset = queryset.filter(
                Q(title__icontains=q) |
                Q(description__icontains=q) |
                Q(content__icontains=q)
            )

        return queryset.order_by('-date_published')

    def get_context_data(self, **kwargs):
        self.logger.debug('PostListView.get_context_data()')
        context = super(PostListView, self).get_context_data(**kwargs)
        context['site_title'] = settings.SITE_TITLE
        return context


class PostDetailView(SearchContextMixin, CategoryContextMixin, DetailView):
    logger = logging.getLogger(__name__)
    queryset = Post.objects.published().select_related('author').select_related('category')
    context_object_name = 'post'
    template_name = 'blog/post_detail.html'

    def get_object(self, queryset=None):
        self.logger.debug('PostDetailView.get_object()')
        object = super(PostDetailView, self).get_object()
        object.view_count += 1
        object.save()
        return object

    def get_context_data(self, **kwargs):
        self.logger.debug('PostDetailView.get_context_data()')
        context = super(PostDetailView, self).get_context_data(**kwargs)
        context['site_title'] = settings.SITE_TITLE
        return context


class PostCategoryView(PageableMixin, SearchContextMixin, CategoryContextMixin, ListView):
    logger = logging.getLogger(__name__)
    context_object_name = 'posts'
    template_name = 'blog/post_category_list.html'

    def get_queryset(self):
        self.logger.debug('PostCategoryView.get_queryset()')
        queryset = Post.objects.published().select_related('author').select_related('category') \
            .filter(category__in=Category.objects.filter(slug=self.kwargs['slug']).get_descendants(include_self=True))
        return queryset.order_by('-date_published')

    def get_context_data(self, **kwargs):
        self.logger.debug('PostCategoryView.get_context_data()')
        context = super(PostCategoryView, self).get_context_data(**kwargs)
        context['site_title'] = settings.SITE_TITLE
        return context


class PostArchiveIndexView(PageableMixin, SearchContextMixin, CategoryContextMixin, ArchiveIndexView):
    queryset = Post.objects.published().order_by('-date_published')
    context_object_name = 'posts'
    template_name = 'blog/post_archive.html'
    date_field = 'date_published'

    def get_context_data(self, **kwargs):
        self.logger.debug('PostArchiveIndexView.get_context_data()')
        context = super(PostArchiveIndexView, self).get_context_data(**kwargs)
        context['site_title'] = settings.SITE_TITLE
        return context


class PostYearArchiveView(PageableMixin, SearchContextMixin, CategoryContextMixin, YearArchiveView):
    queryset = Post.objects.published().order_by('-date_published')
    context_object_name = 'posts'
    template_name = 'blog/post_archive_year.html'
    date_field = 'date_published'
    make_object_list = True

    def get_context_data(self, **kwargs):
        self.logger.debug('PostYearArchiveView.get_context_data()')
        context = super(PostYearArchiveView, self).get_context_data(**kwargs)
        context['site_title'] = settings.SITE_TITLE
        return context


class PostMonthArchiveView(PageableMixin, SearchContextMixin, CategoryContextMixin, MonthArchiveView):
    queryset = Post.objects.published().order_by('-date_published')
    context_object_name = 'posts'
    template_name = 'blog/post_archive_month.html'
    date_field = 'date_published'

    def get_context_data(self, **kwargs):
        self.logger.debug('PostMonthArchiveView.get_context_data()')
        context = super(PostMonthArchiveView, self).get_context_data(**kwargs)
        context['site_title'] = settings.SITE_TITLE
        return context


class PostDayArchiveView(PageableMixin, SearchContextMixin, CategoryContextMixin, DayArchiveView):
    queryset = Post.objects.published().order_by('-date_published')
    context_object_name = 'posts'
    template_name = 'blog/post_archive_day.html'
    date_field = 'date_published'

    def get_context_data(self, **kwargs):
        self.logger.debug('PostDayArchiveView.get_context_data()')
        context = super(PostDayArchiveView, self).get_context_data(**kwargs)
        context['site_title'] = settings.SITE_TITLE
        return context


class TaggingCloudView(SearchContextMixin, CategoryContextMixin, TemplateView):
    template_name = 'blog/tagging_cloud.html'

    def get_context_data(self, **kwargs):
        self.logger.debug('TaggingCloudView.get_context_data()')
        context = super(TaggingCloudView, self).get_context_data(**kwargs)
        context['site_title'] = settings.SITE_TITLE
        return context


class TaggingPostListView(PageableMixin, SearchContextMixin, CategoryContextMixin, TaggedObjectList):
    model = Post
    context_object_name = 'posts'
    template_name = 'blog/tagging_post_list.html'

    def get_context_data(self, **kwargs):
        self.logger.debug('TaggingPostListView.get_context_data()')
        context = super(TaggingPostListView, self).get_context_data(**kwargs)
        context['site_title'] = settings.SITE_TITLE
        return context


class LatestPostFeed(Feed):
    title = _('Blog Feeds')
    link = '/feed'
    description = _('Blog Recent Posts')

    def items(self):
        return Post.objects.published().order_by('-date_published')[:10]

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.description

    def item_link(self, item):
        return reverse('blog:post-detail', args=(item.pk, item.slug,))
