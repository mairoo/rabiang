from django import template
from tagging.models import TaggedItem
from tagging.utils import parse_tag_input

from ..models import Post

register = template.Library()


@register.simple_tag
def get_recent_posts(count=5):
    posts = Post.objects.published().order_by('-date_published')[:count]
    return posts


@register.simple_tag
def get_similar_posts(instance, count=3):
    posts = TaggedItem.objects \
        .get_related(instance, instance.__class__, num=count)
    return posts


@register.simple_tag
def split_tags(input):
    return parse_tag_input(input)
