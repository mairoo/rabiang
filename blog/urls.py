from django.conf.urls import url
from django.views.generic import RedirectView

from .views import (
    PostListView, PostDetailView, PostCategoryView, PostArchiveIndexView, PostYearArchiveView, PostMonthArchiveView,
    PostDayArchiveView,
    TaggingCloudView, TaggingPostListView, LatestPostFeed
)

urlpatterns = [
    url(r'^$',
        RedirectView.as_view(url='/blog/posts'), name='blog-index'),

    url(r'^(?P<pk>\d+)/(?P<slug>[-\w]+)/$',
        PostDetailView.as_view(), name='post-detail'),
    url(r'^posts/$',
        PostListView.as_view(), name='post-list'),

    url(r'^category/(?P<slug>[-\w]+)/$',
        PostCategoryView.as_view(), name='post-category'),

    url(r'^archive/$',
        PostArchiveIndexView.as_view(), name='post-archive'),
    url(r'^archive/(?P<year>\d{4})/$',
        PostYearArchiveView.as_view(), name='post-year-archive'),
    url(r'^archive/(?P<year>[0-9]{4})/(?P<month>[0-9]+)/$',
        PostMonthArchiveView.as_view(month_format='%m'), name='post-month-archive'),
    url(r'^archive/(?P<year>[0-9]{4})/(?P<month>[0-9]+)/(?P<day>[0-9]+)/$',
        PostDayArchiveView.as_view(month_format='%m'), name='post-day-archive'),

    url(r'^tags/$',
        TaggingCloudView.as_view(), name='tagging-cloud'),
    url(r'^tags/(?P<tag>[^/]+(?u))/$',
        TaggingPostListView.as_view(), name='tagging-post-list'),

    url(r'^feed/$',
        LatestPostFeed(), name='post-feed'),
]
