from django.contrib import admin
from django.utils import timezone
from ipware.ip import get_ip
from mptt.admin import MPTTModelAdmin

from .forms import PostAdminForm
from .models import (
    Post, Image, Category
)


class ImageInline(admin.StackedInline):
    model = Image
    extra = 1


class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'author', 'date_published', 'status', 'ip_address')
    list_filter = ('status', 'created', 'date_published', 'author')
    search_fields = ('title', 'content')
    prepopulated_fields = {'slug': ('title',)}
    date_hierarchy = 'date_published'
    ordering = ['status', '-date_published']
    raw_id_fields = ('author',)
    exclude = ('date_published', 'ip_address', 'view_count')
    inlines = [ImageInline]
    form = PostAdminForm

    class Media:
        css = {
            'all': ('css/admin/simplemde.css',)
        }

    def save_model(self, request, obj, form, change):

        if 'status' in form.changed_data \
                and obj.status == Post.PUBLISHED \
                and obj.date_published is None:
            obj.date_published = timezone.now()

        if obj.id is None:
            obj.ip_address = get_ip(request)

        super().save_model(request, obj, form, change)


class ImageAdmin(admin.ModelAdmin):
    list_display = ('post', 'image', 'created')


class CategoryAdmin(MPTTModelAdmin):
    list_display = ('title', 'slug', 'position')
    prepopulated_fields = {'slug': ('title',)}
    mptt_level_indent = 20


admin.site.register(Post, PostAdmin)
admin.site.register(Image, ImageAdmin)
admin.site.register(Category, CategoryAdmin)
