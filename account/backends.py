from hashlib import sha1

from .models import User


class OpencartBackend:
    # Create an authentication method
    # This is called by the standard Django login procedure
    def authenticate(self, username=None, password=None):

        try:
            # When email is username
            user = User.objects.get(email=username)

            # Salted password SHA1 hashing 3 iterations
            hashed = sha1(
                (user.salt + sha1(
                    (user.salt + sha1(
                        password.encode('utf8')
                    ).hexdigest()).encode('utf8')
                ).hexdigest()).encode('utf8')
            ).hexdigest()

            if user.password == hashed:
                return user
            else:
                return None

        except User.DoesNotExist:
            return None

    # Required for your backend to work properly - unchanged in most scenarios
    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
