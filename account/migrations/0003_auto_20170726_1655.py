# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-26 07:55
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0002_userloginlog'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='userloginlog',
            options={'ordering': ('-created',), 'verbose_name': 'user login log', 'verbose_name_plural': 'user login logs'},
        ),
        migrations.AlterModelTable(
            name='userloginlog',
            table='user_login_log',
        ),
    ]
