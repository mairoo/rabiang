import logging

from django.conf import settings
from django.views.generic.base import TemplateView


class PageDetailView(TemplateView):
    logger = logging.getLogger(__name__)

    def get_context_data(self, **kwargs):
        self.logger.debug('PageDetailView.get_context_data()')
        context = super(PageDetailView, self).get_context_data(**kwargs)
        context['site_title'] = settings.SITE_TITLE
        return context

    def get_template_names(self):
        self.logger.debug('PageDetailView.get_template_names()')
        return ['page/%s.html' % self.kwargs['slug']]
