from django.db import models


class MessageQuerySet(models.QuerySet):
    def board(self, slug):
        return self.filter(board__slug=slug)

    def author(self, author):
        return self.filter(author=author)

    def published(self):
        return self.filter(status=self.model.PUBLISHED)

    def public(self):
        return self.filter(is_secret=False)

    def secret(self):
        return self.filter(is_secret=True)

    def deleted(self):
        return self.filter(status=self.model.DELETED)


class MessageManager(models.Manager):
    def get_queryset(self):
        return MessageQuerySet(self.model, using=self._db)

    def board(self, slug):
        return self.get_queryset().board(slug)

    def author(self, author):
        return self.get_queryset().author(author)

    def published(self):
        return self.get_queryset().published()

    def public(self):
        return self.get_queryset().public()

    def secret(self):
        return self.get_queryset().secret()

    def deleted(self):
        return self.get_queryset().deleted()
