from django.contrib import admin
from ipware.ip import get_ip
from mptt.admin import MPTTModelAdmin

from .models import (
    Board, Message, Image, Category
)


class ImageInline(admin.StackedInline):
    model = Image
    extra = 1


class BoardAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'created')
    ordering = ['-created']


class MessageAdmin(admin.ModelAdmin):
    list_display = ('board', 'title', 'author', 'created', 'status', 'is_secret', 'ip_address')
    list_display_links = ('title',)
    list_filter = ('board__title', 'status', 'created', 'author')
    search_fields = ('title', 'content')
    ordering = ['-created']
    raw_id_fields = ('author',)
    exclude = ('password', 'view_count', 'ip_address',)
    inlines = [ImageInline]

    def save_model(self, request, obj, form, change):
        if obj.id is None:
            obj.ip_address = get_ip(request)

        super().save_model(request, obj, form, change)

    def get_form(self, request, obj=None, **kwargs):
        request.current_object = obj
        return super(MessageAdmin, self).get_form(request, obj, **kwargs)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        instance = request.current_object

        if db_field.name == 'category':
            if hasattr(instance, 'board'):  # Board is not specified when writing a new message
                kwargs['queryset'] = Category.objects.filter(board=instance.board)
        return super(MessageAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


class ImageAdmin(admin.ModelAdmin):
    list_display = ('message', 'image', 'created')


class CategoryAdmin(MPTTModelAdmin):
    list_display = ('title', 'slug', 'position', 'board')
    list_filter = ['board__title', 'created']
    prepopulated_fields = {'slug': ('title',)}
    mptt_level_indent = 20


admin.site.register(Board, BoardAdmin)
admin.site.register(Message, MessageAdmin)
admin.site.register(Image, ImageAdmin)
admin.site.register(Category, CategoryAdmin)
