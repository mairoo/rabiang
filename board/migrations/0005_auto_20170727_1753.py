# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-27 08:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('board', '0004_auto_20170727_1753'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='is_secret',
            field=models.BooleanField(default=False, help_text='Whether to be secret or not', verbose_name='Secret'),
        ),
    ]
