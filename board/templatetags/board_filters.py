from django import template
from django.template.defaultfilters import stringfilter
from django.utils import timezone

register = template.Library()


@register.filter
@stringfilter
def mask_ip_address(ip):
    ip_class = ip.split('.')
    ip_class[1] = 'xxx'
    return '.'.join(ip_class)


@register.filter
def is_today(dt):
    return timezone.now().today().date() == timezone.localtime(dt).date()
