import json
import logging
import urllib

from django import forms
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from .models import Message


class MessageForm(forms.ModelForm):
    logger = logging.getLogger(__name__)

    title = forms.CharField(
        label=_('Title'),
        required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Title'),
                'required': 'True',
            }
        )
    )
    content = forms.CharField(
        label=_('Content'),
        required=True,
        widget=forms.Textarea(
            attrs={
                'class': 'form-control',
                'rows': 15,
                'placeholder': _('Content'),
                'required': 'True',
            }
        )
    )
    is_secret = forms.BooleanField(
        label=_('Secret'),
        required=False,
        initial=False,
    )

    class Meta:
        model = Message
        fields = ['title', 'content', 'is_secret']

    def __init__(self, *args, **kwargs):
        self.logger.debug('MessageForm.__init__()')
        # important to "pop" added kwarg before call to parent's constructor
        self.request = kwargs.pop('request')
        super(MessageForm, self).__init__(*args, **kwargs)


class AnonymousMessageForm(MessageForm):
    logger = logging.getLogger(__name__)

    nickname = forms.CharField(
        label=_('Nickname'),
        required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Nickname'),
                'required': 'True',
            }
        )
    )
    password = forms.CharField(
        label=_('Password'),
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Password'),
                'required': 'True',
            }
        )
    )

    class Meta:
        model = Message
        fields = ['title', 'content', 'nickname', 'password', 'is_secret']

    def clean(self):
        self.logger.debug('AnonymousMessageForm.clean()')
        # Google reCAPTCHA
        ''' Begin reCAPTCHA validation '''
        recaptcha_response = self.request.POST.get('g-recaptcha-response')
        url = 'https://www.google.com/recaptcha/api/siteverify'
        values = {
            'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
            'response': recaptcha_response
        }
        data = urllib.parse.urlencode(values).encode()
        req = urllib.request.Request(url, data=data)
        response = urllib.request.urlopen(req)
        result = json.loads(response.read().decode())
        ''' End reCAPTCHA validation '''

        if not result['success']:
            raise forms.ValidationError(_('reCAPTCHA error occurred.'))

        return super(AnonymousMessageForm, self).clean()


class SecretMessageForm(forms.Form):
    password = forms.CharField(
        label=_('Password'),
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Password'),
                'required': 'True',
            }
        )
    )


class MessageSearchForm(forms.Form):
    TITLE = 'title'
    CONTENT = 'content'
    TITLE_CONTENT = 'title_content'
    NICKNAME = 'nickname'

    where = forms.ChoiceField(
        label=_('Search Fields'),
        widget=forms.Select(
            attrs={
                'class': 'form-control',
                'required': True,
            }
        ),
        choices=([
            (TITLE, _('Title')),
            (CONTENT, _('Content')),
            (TITLE_CONTENT, _('Title+Content')),
            (NICKNAME, _('Nickname')),
        ]),
        initial=TITLE_CONTENT,
        required=True,
    )
    q = forms.CharField(
        label=_('Search Word'),
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': _('Search Word'),
                'required': 'True',
            }
        )
    )
