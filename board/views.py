import logging

from django.conf import settings
from django.contrib.auth.hashers import (
    make_password, check_password
)
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.views.generic import (
    ListView, DetailView
)
from django.views.generic.edit import (
    CreateView, UpdateView, DeleteView, FormMixin
)
from ipware.ip import get_ip

from .forms import (
    MessageForm, AnonymousMessageForm, SecretMessageForm, MessageSearchForm
)
from .models import (
    Message
)
from .viewmixins import (
    PageableMixin, BoardContextMixin
)


class MessageListView(BoardContextMixin, PageableMixin, ListView):
    logger = logging.getLogger(__name__)
    context_object_name = 'messages'
    template_name = 'board/message_list.html'
    form_class = MessageSearchForm

    def __init__(self):
        self.logger.debug('MessageListView.__init__()')
        super(MessageListView, self).__init__()
        self.block_size = 10  # default value

    def get_queryset(self):
        self.logger.debug('MessageListView.get_queryset()')
        form = self.form_class(self.request.GET)
        queryset = Message.objects.published() \
            .select_related('author') \
            .select_related('board') \
            .board(self.kwargs['slug'])

        if form.is_valid():
            where = form.cleaned_data['where']
            q = form.cleaned_data['q']

            queryset = queryset.public()  # search only in public messages

            if where == form.TITLE:
                queryset = queryset.filter(Q(title__icontains=q) | Q(content__icontains=q))
            elif where == form.CONTENT:
                queryset = queryset.filter(Q(content__icontains=q))
            elif where == form.TITLE_CONTENT:
                queryset = queryset.filter(Q(title__icontains=q) | Q(content__icontains=q))
            elif where == form.NICKNAME:
                queryset = queryset.filter(Q(nickname__icontains=q))

        return queryset.order_by('-created')

    def get_context_data(self, **kwargs):
        self.logger.debug('MessageListView.get_context_data()')
        context = super(MessageListView, self).get_context_data(**kwargs)
        context['site_title'] = settings.SITE_TITLE
        context['board'] = self.board  # already fetched by BoardContextMixin.dispatch()
        context['form'] = self.form_class
        return context

    def get_paginate_by(self, queryset):
        self.logger.debug('MessageListView.get_paginate_by()')
        self.block_size = self.board.block_size
        return self.board.chunk_size


class MessageDetailView(BoardContextMixin, FormMixin, DetailView):
    logger = logging.getLogger(__name__)
    context_object_name = 'message'
    form_class = SecretMessageForm
    template_name = 'board/message_detail.html'
    unlocked = False

    def post(self, request, *args, **kwargs):
        self.logger.debug('MessageDetailView.post()')
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_object(self, queryset=None):
        self.logger.debug('MessageDetailView.get_object()')
        o = super(MessageDetailView, self).get_object()
        o.view_count += 1
        o.save()
        return o

    def get_queryset(self):
        self.logger.debug('MessageDetailView.get_queryset()')
        return Message.objects.published() \
            .select_related('author') \
            .select_related('board') \
            .board(self.kwargs['slug'])

    def get_context_data(self, **kwargs):
        self.logger.debug('MessageDetailView.get_context_data()')
        context = super(MessageDetailView, self).get_context_data(**kwargs)
        context['site_title'] = settings.SITE_TITLE
        context['board'] = self.board  # already fetched by BoardContextMixin.dispatch()
        context['hidden'] = False

        if self.object.is_secret and self.request.user != self.object.author and not self.unlocked:
            context['form'] = self.get_form()
            context['hidden'] = True

        return context

    def form_valid(self, form):
        self.logger.debug('MessageDetailView.form_valid()')

        if check_password(form.cleaned_data['password'], self.object.password):
            self.unlocked = True

            # success_url is not specified because it shows the message by itself.
            return self.render_to_response(self.get_context_data(form=form))

        return super(MessageDetailView, self).form_valid(form)


class MessageCreateView(BoardContextMixin, CreateView):
    logger = logging.getLogger(__name__)
    template_name = 'board/message_form.html'

    def get_context_data(self, **kwargs):
        self.logger.debug('MessageCreateView.get_context_data()')
        context = super(MessageCreateView, self).get_context_data(**kwargs)
        context['site_title'] = settings.SITE_TITLE
        context['board'] = self.board  # already fetched by BoardContextMixin.dispatch()
        context['google_recaptcha_site_key'] = settings.GOOGLE_RECAPTCHA_SITE_KEY
        return context

    def get_form_class(self):
        self.logger.debug('MessageCreateView.get_form_class()')

        if self.request.user.is_authenticated():
            return MessageForm
        else:
            return AnonymousMessageForm

    def get_form_kwargs(self):
        # overrides to pass 'self.request' object
        self.logger.debug('MessageCreateView.get_form_kwargs()')
        kwargs = super(MessageCreateView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_initial(self):
        self.logger.debug('MessageCreateView.get_initial()')
        initial = super(MessageCreateView, self).get_initial()
        initial['board'] = self.board
        return initial

    def form_valid(self, form):
        self.logger.debug('MessageCreateView.form_valid()')

        form.instance.board = self.board
        form.instance.ip_address = get_ip(self.request)

        if self.request.user.is_authenticated():
            form.instance.author = self.request.user
            form.instance.nickname = self.request.user.nickname
        else:
            form.instance.author = None
            form.instance.password = form.cleaned_data['password'] = make_password(form.cleaned_data['password'])

        # The following fields are filled by default:
        # created, updated, status, view_count
        return super(MessageCreateView, self).form_valid(form)

    def get_success_url(self):
        self.logger.debug('MessageCreateView.get_success_url()')
        return reverse('board:message-detail', args=(self.object.board.slug, self.object.id,))


class MessageChangeListView(LoginRequiredMixin, PageableMixin, ListView):
    logger = logging.getLogger(__name__)
    context_object_name = 'messages'
    template_name = 'board/message_change_list.html'
    paginate_by = 20
    block_size = 10

    def get_queryset(self):
        self.logger.debug('MessageChangeListView.get_queryset()')
        return Message.objects \
            .select_related('author') \
            .select_related('board') \
            .author(self.request.user) \
            .order_by('-created')

    def get_context_data(self, **kwargs):
        self.logger.debug('MessageChangeListView.get_context_data()')
        context = super(MessageChangeListView, self).get_context_data(**kwargs)
        context['site_title'] = settings.SITE_TITLE
        return context


class MessageUpdateView(BoardContextMixin, UpdateView):
    logger = logging.getLogger(__name__)
    model = Message
    template_name = 'board/message_form.html'

    def get_context_data(self, **kwargs):
        self.logger.debug('MessageUpdateView.get_context_data()')
        context = super(MessageUpdateView, self).get_context_data(**kwargs)
        context['site_title'] = settings.SITE_TITLE
        context['board'] = self.board  # already fetched by BoardContextMixin.dispatch()
        return context

    def get_form_class(self):
        self.logger.debug('MessageUpdateView.get_form_class()')

        if self.request.user.is_authenticated():
            return MessageForm
        else:
            return AnonymousMessageForm

    def get_form_kwargs(self):
        # overrides to pass 'self.request' object
        self.logger.debug('MessageUpdateView.get_form_kwargs()')
        kwargs = super(MessageUpdateView, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def get_success_url(self):
        self.logger.debug('MessageUpdateView.get_success_url()')
        return reverse('board:message-detail', args=(self.object.board.slug, self.object.id,))


class MessageDeleteView(BoardContextMixin, DeleteView):
    logger = logging.getLogger(__name__)
    model = Message
    context_object_name = 'message'
    template_name = 'board/message_confirm_delete.html'

    def get_context_data(self, **kwargs):
        self.logger.debug('MessageDeleteView.get_context_data()')
        context = super(MessageDeleteView, self).get_context_data(**kwargs)
        context['site_title'] = settings.SITE_TITLE
        context['board'] = self.board  # already fetched by BoardContextMixin.dispatch()
        return context

    def get_success_url(self):
        self.logger.debug('MessageDeleteView.get_success_url()')
        return reverse('board:message-list', args=(self.object.board.slug,))
