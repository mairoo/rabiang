from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import pgettext_lazy
from django.utils.translation import ugettext_lazy as _
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel

from model_utils.models import TimeStampedModel
from .managers import MessageManager


class Board(TimeStampedModel):
    title = models.CharField(
        verbose_name=_('Title'),
        help_text=_('Enter board title.'),
        max_length=250
    )
    slug = models.SlugField(
        verbose_name=_('Slug'),
        help_text=_('Enter board slug.'),
        max_length=250,
        unique=False,
        allow_unicode=True
    )
    allow_comments = models.BooleanField(
        verbose_name=_('Allow comments'),
        default=True
    )
    chunk_size = models.PositiveIntegerField(
        verbose_name=_('Pagination chunk size'),
        default=20
    )
    block_size = models.PositiveIntegerField(
        verbose_name=_('Pagination block size'),
        default=10
    )

    class Meta:
        verbose_name = _('board')
        verbose_name_plural = _('boards')
        ordering = ['created']

    def __str__(self):
        return self.title


class Message(TimeStampedModel):
    PUBLISHED = 0
    DELETED = 1

    STATUS_CHOICES = (
        (PUBLISHED, _('Published')),
        (DELETED, _('Deleted')),
    )

    board = models.ForeignKey(
        Board,
        verbose_name=_('Board'),
        related_name='board_messages',
    )
    category = TreeForeignKey(
        'Category',
        verbose_name=_('Category'),
        help_text=_('Enter board message category.'),
        null=True,
        blank=True,
        db_index=True
    )
    title = models.CharField(
        verbose_name=_('Title'),
        help_text=_('Enter board message title.'),
        max_length=250
    )
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_('Author'),
        help_text=_('Specify board message author.'),
        related_name='author_messages',
        blank=True,
        null=True
    )
    nickname = models.CharField(
        verbose_name=_('Nickname'),
        max_length=30,
        blank=True
    )
    password = models.CharField(
        _('Password'),
        max_length=128,
        blank=True
    )
    content = models.TextField(
        verbose_name=_('Content'),
        help_text = _('Enter board message content.'),
    )
    is_secret = models.BooleanField(
        verbose_name=_('Secret'),
        help_text=_('Whether to be secret or not'),
        default=False
    )
    ip_address = models.GenericIPAddressField(
        verbose_name=_('IP Address')
    )
    view_count = models.PositiveIntegerField(
        verbose_name=_('View Count'),
        default=0
    )

    status = models.IntegerField(
        verbose_name=_('Status'),
        choices=STATUS_CHOICES,
        default=PUBLISHED
    )

    objects = MessageManager()

    class Meta:
        verbose_name = pgettext_lazy('Board', 'message')
        verbose_name_plural = pgettext_lazy('Board', 'messages')
        ordering = ('-created',)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('board:message-detail', args=(self.board.slug, self.pk,))


class Image(TimeStampedModel):
    message = models.ForeignKey(
        Message,
        verbose_name=_('Message'),
        related_name='board_messages'
    )
    image = models.ImageField(
        verbose_name=_('Image'),
        upload_to='blog/images/%Y/%m'
    )

    class Meta:
        verbose_name = _('image')
        verbose_name_plural = _('images')
        ordering = ['created']

    def __str__(self):
        return self.image.name


class Category(TimeStampedModel, MPTTModel):
    board = models.ForeignKey(
        'Board',
        verbose_name=_('Board'),
        related_name='board'
    )
    parent = TreeForeignKey(
        'self',
        verbose_name=_('Parent'),
        blank=True,
        null=True,
        related_name='children',
        db_index=True
    )
    title = models.CharField(
        verbose_name=_('Title'),
        max_length=128
    )
    slug = models.SlugField(
        verbose_name=_('Slug'),
        max_length=250,
        unique=True,
        allow_unicode=True
    )
    position = models.IntegerField(
        verbose_name=_('Position'),
        default=1,
    )

    class Meta:
        verbose_name = _('category')
        verbose_name_plural = _('categories')
        ordering = ['tree_id', 'lft']

    class MPTTMeta:
        order_insertion_by = ['position']

    def __str__(self):
        return self.title
