import logging

from .models import Board


class BoardContextMixin(object):
    logger = logging.getLogger(__name__)

    def dispatch(self, *args, **kwargs):
        self.logger.debug('BoardContextMixin.dispatch()')
        self.board = Board.objects.get(slug=self.kwargs.get('slug'))
        return super(BoardContextMixin, self).dispatch(*args, **kwargs)


class PageableMixin(object):
    logger = logging.getLogger(__name__)

    def get_context_data(self, **kwargs):
        self.logger.debug('PageableMixin.get_context_data()')
        context = super(PageableMixin, self).get_context_data(**kwargs)

        start_index = int((context['page_obj'].number - 1) / self.block_size) * self.block_size
        end_index = min(start_index + self.block_size, len(context['paginator'].page_range))

        context['page_range'] = context['paginator'].page_range[start_index:end_index]
        return context
