(function ($) {
    $(document).on('formset:added', function (event, $row, formsetName) {
        // do something when inline formset row added
    });

    $(document).on('formset:removed', function (event, $row, formsetName) {
        // do something when inline formset row removed
    });
})(django.jQuery);

