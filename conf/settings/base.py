"""
Django settings for Rabiang project.

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.11/ref/settings/

Deployment checklist for production, see
https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/
"""

import os

try:
    from .secret import *
except ImportError:
    raise ImportError(
        'conf.settings.secret module does not exist.'
    )

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
# BASE_DIR is the parent directory not the current directory.
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# SECURITY WARNING: Keep them in secret!
SECRET_KEY = Secret.SECRET_KEY
DEBUG = Secret.DEBUG
ALLOWED_HOSTS = Secret.ALLOWED_HOSTS
DATABASES = Secret.DATABASES

# Application definition
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'conf.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'debug': DEBUG,
        },
    },
]

WSGI_APPLICATION = 'conf.wsgi.application'

# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator', },
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator', },
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator', },
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator', },
]

# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/
LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

USE_I18N = True
USE_L10N = True
USE_TZ = True

# Authentication
# Custom auth user model
AUTH_USER_MODEL = 'account.User'

# Authentication Backend
# AUTHENTICATION_BACKENDS = ('account.backends.OpencartBackend',)

# django.contrib.auth
LOGIN_URL = '/accounts/login/'  # default
LOGOUT_URL = '/accounts/logout/'  # default
LOGIN_REDIRECT_URL = '/'  # default = /accounts/profile/
# LOGOUT_REDIRECT_URL = '/'
PASSWORD_RESET_TIMEOUT_DAYS = 1  # default = 1 (password reset and user activation timeout)
EMAIL_ACCOUNT_ACTIVATION = False

# Email
EMAIL_BACKEND = Secret.EMAIL_BACKEND
EMAIL_HOST = Secret.EMAIL_HOST
EMAIL_PORT = Secret.EMAIL_PORT
EMAIL_HOST_USER = Secret.EMAIL_HOST_USER
EMAIL_HOST_PASSWORD = Secret.EMAIL_HOST_PASSWORD
EMAIL_USE_TLS = Secret.EMAIL_USE_TLS

# Google reCAPTCHA
GOOGLE_RECAPTCHA_SECRET_KEY = Secret.GOOGLE_RECAPTCHA_SECRET_KEY
GOOGLE_RECAPTCHA_SITE_KEY = Secret.GOOGLE_RECAPTCHA_SITE_KEY

# Disqus
# DISQUS_WEBSITE_SHORTNAME = 'your_website_shortname'
