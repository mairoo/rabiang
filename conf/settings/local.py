"""
1. Create your own local.py, staging.py or production.py.
2. Set environment variable correctly. (DJANGO_SETTINGS_MODULE=conf.settings.local)
"""

from django.utils.translation import ugettext_lazy as _

from .base import *

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_URL = '/assets/'
STATIC_ROOT = os.path.join(BASE_DIR, 'assets/')
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static/'),
]

# Media files (Upload)
# https://docs.djangoproject.com/en/1.10/topics/files/

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')

# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

TIME_ZONE = 'Asia/Seoul'

LANGUAGE_CODE = 'ko-kr'
LANGUAGES = [
    ('en', _('English')),
    ('ko', _('Korean')),
]

# django.contrib.sites is required for disqus and other applications.
SITE_ID = 1
SITE_TITLE = 'Rabiang'

INSTALLED_APPS += [
    # Additional Django package
    'django.contrib.sites',

    # Third-party apps
    'sitetree',
    'tagging.apps.TaggingConfig',
    'mptt',
    'easy_thumbnails',
    # 'disqus',

    # my custom app
    'rabiang_editor',

    # Project own apps
    'account.apps.AccountConfig',
    'navbar.apps.NavbarConfig',
    'blog.apps.BlogConfig',
    'board.apps.BoardConfig',
    'page.apps.PageConfig',

    # Comment
    'django_comments_xtd',
    'django_comments',
]

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
        }
    },
    'loggers': {
        'django.db.backends': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
        'account': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
        'blog': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
        'board': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
        'page': {
            'handlers': ['console'],
            'level': 'DEBUG',
        },
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'unique-snowflake',
        'TIMEOUT': 300,  # default = 300 seconds(5 minutes)
        'OPTIONS': {
            'MAX_ENTRIES': 300  # default = 300
        }
    }
}

COMMENTS_APP = 'django_comments_xtd'
COMMENTS_XTD_MAX_THREAD_LEVEL = 1

SIMPLEMDE_OPTIONS = {
    'autofocus': False,
    'forceSync': True,
    'hideIcons': ['preview', 'side-by-side'],
    'indentWithTabs': False,
    'lineWrapping': True,
    'promptURLs': True,
    'renderingConfig': {
        'singleLineBreaks': False,
        'codeSyntaxHighlighting': True
    },
    'showIcons': ['code', 'table', 'strikethrough', 'horizontal-rule'],
    'spellChecker': False,
}
