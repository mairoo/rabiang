from django.conf import settings
from django.conf.urls import (
    url, include
)
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import RedirectView

from account.views import UserLoginView

urlpatterns = [
    # Index page
    url(r'^$',
        RedirectView.as_view(url='/page/index'), name='index'),

    # Project own apps
    url(r'^blog/',
        include('blog.urls', namespace='blog')),
    url(r'^board/',
        include('board.urls', namespace='board')),
    url(r'^page/',
        include('page.urls', namespace='page')),
    url(r'^accounts/',
        include('account.urls', namespace='account')),

    url(r'^comments/',
        include('django_comments_xtd.urls')),

    # Admin site
    # Redirect default admin login page into custom login page
    url(r'admin/login/',
        UserLoginView.as_view()),
    url(r'^admin/',
        admin.site.urls),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
